#ifndef GEMM_BLOCK_H_GUARD
#define GEMM_BLOCK_H_GUARD

#define BLOCK 64
#define BLOCK_SIZE BLOCK * BLOCK * sizeof(float)

float** create_block_matrix(int M, int N, float* A);
float* get_matrix_from_block(int M, int N, float** A);
void conv_block_to_matrix(int M, int N, float** A, float* a);
void release_block_matrix(int M, int N, float** A);
void gemm_block(int M, int N, int K, float** A, float** B, float** C);
void gemm_ref(int M, int N, int K, float *A, float *B, float *C);
void print_block_matrix(int M, int N, float** A);
void print_regular_matrix(int M, int N, float* A);
void gemm_hw(float* A, float* B, float* C);
void gemm_partition(int TA, int TB, int M, int N, int K, float ALPHA,
          float *A, int lda,
          float *B, int ldb,
          float BETA,
          float *C, int ldc);

#endif
