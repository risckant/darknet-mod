/*
 * Copyright (c) 2012 Xilinx, Inc.  All rights reserved.
 *
 * Xilinx, Inc.
 * XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS" AS A
 * COURTESY TO YOU.  BY PROVIDING THIS DESIGN, CODE, OR INFORMATION AS
 * ONE POSSIBLE   IMPLEMENTATION OF THIS FEATURE, APPLICATION OR
 * STANDARD, XILINX IS MAKING NO REPRESENTATION THAT THIS IMPLEMENTATION
 * IS FREE FROM ANY CLAIMS OF INFRINGEMENT, AND YOU ARE RESPONSIBLE
 * FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE FOR YOUR IMPLEMENTATION.
 * XILINX EXPRESSLY DISCLAIMS ANY WARRANTY WHATSOEVER WITH RESPECT TO
 * THE ADEQUACY OF THE IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO
 * ANY WARRANTIES OR REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE
 * FROM CLAIMS OF INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.
 *
 */
#ifdef ACCEL

#include "xhls_accel.h"
#include "libaxidma/libaxidma.h"
#include "gemm_block.h"
#include <stdint.h>
#include <string.h>

XHls_accel accel;
axidma_dev_t dma_dev;

#define LEN 32*32*4
#define DIM 32

void matrix_multiply_ref(float* a, float* b, float* c, float* out)
{

	int ia, ib, id;

  // matrix multiplication of a A*B matrix
  for (ia = 0; ia < DIM; ++ia)
     for (ib = 0; ib < DIM; ++ib)
     {

    	 float sum = c[ia * DIM + ib];

		 for (id = 0; id < DIM; ++id)

			 sum += a[ia * DIM + id] * b[id * DIM + ib];

		 out[ia * DIM + ib] = sum;
     }
}

int test_main()
{


    return 0;
}

#endif
